#include <stdio.h>
#include "dice.h"

int main() {
    // create a seed for the random function used in the rollDice()
    initializeSeed();

    int faces;
    printf("How many faces will your dice have? ");
    scanf("%d", &faces);

    // generate a random number from 1 to 6, as if it were a non-addicted dice
    printf("Let's roll the dice: %d\n", rollDice(faces));
    return 0;
}
